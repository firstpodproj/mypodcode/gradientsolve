//
//  CAGradientLayer+ClearContent.m
//  PNCBank
//
//  Created by a201111 on 2021/11/17.
//  Copyright © 2021 T&Y Information. All rights reserved.
//

#import "CAGradientLayer+ClearContent.h"
#import <objc/runtime.h>

@implementation CAGradientLayer (ClearContent)

+ (BOOL)resolveInstanceMethod:(SEL)sel {
    if (sel == NSSelectorFromString(@"_clearContents")) {
        class_addMethod([self class], sel, (IMP)overClearcontents, "v@:");//动态添加方法
        return YES;
    }
    return [super resolveInstanceMethod:sel];
}
void overClearcontents(id obj, SEL _cmd) {
    NSLog(@"CAGradientLayer could not find method");
}
@end
