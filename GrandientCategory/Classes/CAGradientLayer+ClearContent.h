//
//  CAGradientLayer+ClearContent.h
//  PNCBank
//
//  Created by a201111 on 2021/11/17.
//  Copyright © 2021 T&Y Information. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CAGradientLayer (ClearContent)

@end

NS_ASSUME_NONNULL_END
