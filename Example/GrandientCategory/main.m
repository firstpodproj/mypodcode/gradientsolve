//
//  main.m
//  GrandientCategory
//
//  Created by TYF on 01/10/2022.
//  Copyright (c) 2022 TYF. All rights reserved.
//

@import UIKit;
#import "TYFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TYFAppDelegate class]));
    }
}
